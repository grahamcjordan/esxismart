
@{

    # Version number of this module.
    ModuleVersion = '3.0.0'

    # ID used to uniquely identify this module
    GUID = 'fa35db41-3b6d-4df7-8ed2-7059b0ac57f1'

    # Author of this module
    Author = 'Graham Jordan'

    # Copyright statement for this module
    Copyright = '(c) 2020 Graham Jordan. All rights reserved. Open source the hell out of this if you want, go crazy.'

    # Description of the functionality provided by this module
    Description = 'PowerShell Module to monitor ESXi SMART data'

    # Modules that must be imported into the global environment prior to importing this module
    RequiredModules = @(
        'VMware.Vim'                                                                                                                                                                            
        'VMware.VimAutomation.Cis.Core'
        'VMware.VimAutomation.Common'
        'VMware.VimAutomation.Core'
        'VMware.VimAutomation.Sdk'
    )

    # Modules to import as nested modules of the module specified in RootModule/ModuleToProcess
    NestedModules = @(
        'Start-SmartDataScript',            # Root Function: Calls all separate functions.
        'Add-ESXiHardwareData',             # Version 3: Adds CPU, Memory and DataStore info to table ESXiData.
        'Connect-ESXi',                     # Version 1 & 3: Connects to ESXi.
        'Connect-MySQLforSmart',            # Version 1 & 3: Connects to MySQL. Updated to include SimplySQL from PS Gallery.
        'Get-ExtendedSmartData',            # Version 1: Combines SMART data with Config Data.
        'Get-SmartData',                    # Version 1: Gets Hard Drive SMART data.
        'Get-VMDiskInformation',            # Version 1: Gets Hard Drive config data.
        'Initialize-SQLSmartDatabase',      # Version 1-3: Builds or updates database and tables.
        'Add-ToSmartDB',                    # Version 1: Adds ExtendedSmartData to database.
        'Start-AlertChecks',                # Version 2: Checks to see if any temp breaches or drive failures have occured.
        'Send-AlertEmail',                  # Version 2: Sends email alerts if breaches have occured.
        'Start-EmailValidation'             # Version 2: Validates email address and password settings.
        'New-SmartScheduledTask'            # Version 3: Builds a schedule task 
    )

    # Cmdlets to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no cmdlets to export.
    CmdletsToExport = @()

    # Variables to export from this module
    VariablesToExport = '*'

    # Aliases to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no aliases to export.
    AliasesToExport = @()

    # Private data to pass to the module specified in RootModule/ModuleToProcess. This may also contain a PSData hashtable with additional module metadata used by PowerShell.
    PrivateData = @{

        PSData = @{

            # Tags applied to this module. These help with module discovery in online galleries.
            # Tags = @()

            # A URL to the license for this module.
            # LicenseUri = ''

            # A URL to the main website for this project.
            ProjectUri = 'https://bitbucket.org/grahamcjordan/esxismart/'

            # A URL to an icon representing this module.
            # IconUri = ''

            # ReleaseNotes of this module
            # ReleaseNotes = ''

        } # End of PSData hashtable

    } # End of PrivateData hashtable

}

