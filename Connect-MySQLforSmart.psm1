

function Connect-MySQLforSmart {

    <#

        .SYNOPSIS
            Connects to MySQL.

        .DESCRIPTION
            Sets execution policy to unrestricted, loads MySQL assembly types and connects to MySQL.

        .PARAMETER SQLUser
            Username for MySQL. Defaults to Root.

        .PARAMETER SQLPass
            [MANDATORY] Password for SQLUser.

        .PARAMETER SQLHost
            IP or hostname for computer with MySQL installed. Defaults to localhost.
        
        .PARAMETER SQLPort
            Port to connect MySQL server. Defaults to 3306.

        .PARAMETER SQLSslType
            If your database is secure, an ssl encryption method is required. The default is none.

        .EXAMPLE
            Connect-MySQL -SQLPass ChangeMe! -SQLHost Synology -Port 3307

            Connects to MySQL on the local network device 'Synology' on port 3307 (MariaDB10).

        .EXAMPLE
            Connect-MySQL -SQLPass ChangeMe!

            Connects to MySQL on a local machine: most likely XAMPP

        .NOTES
            The default setup has XAMPP users in mind. The default port of MySQL on version 3.2.4 (testing version),
            is 3306. If this isn't working, check C:\xampp\mysql\bin\my.ini and look for port=
            If you're using a Synology device version 6 onwards you're likely using MariaDB10. In which case the port
            has changed to 3307. Either update the param below or set the param in the function call.

            Author: Graham Jordan

        TODO
            One should consider setting up the database, and making it require SSL by default. To look into.

    #>

    [cmdletbinding()]

    param(

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLUser = 'root',

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [securestring]$SQLPass,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLHost = '127.0.0.1',

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [int]$SQLPort = 3306,

        [Parameter( Mandatory = $false )]
        [ValidateSet( 'None', 'Preferred', 'Required')]
        [string]$SQLSslType = "None"

    )

    $sqlCredentials = New-Object -TypeName pscredential( $SQLUser, $SQLPass )
    $sqlUnsecurePass = $sqlCredentials.GetNetworkCredential().Password

    # Installs SimplySQL Module.
    if ( !( Get-Module -Name SimplySql -ListAvailable ) ){
        try {
            Install-Module -Name SimplySql -Force -AcceptLicense -ErrorAction Stop 
        } catch {
            Write-Error "Unable to install the 'SimplySQL' module from the PowerShell Gui. `nError: $( $_.Exception.Message 
                ) `nPlease use 'Install-Module -Name SimplySql -Force -AcceptLicense' from an elevated PowerShell Window."

            Start-Sleep -Seconds 5
            Exit
        }
    }

    # Imports SimplySQL mofule
    Import-Module -Name SimplySql -Force -Global

    # database connection string. Cannot use securestring or credential object when making a connection.
    $sqlConnection = [MySql.Data.MySqlClient.MySqlConnection]@{
        ConnectionString="server=$SqlHost;port=$SQLPort;uid=$SQLUser;pwd=$sqlUnsecurePass;SslMode=none" }

    Return $sqlConnection

}
