
function Get-SmartData {

    <#

        .SYNOPSIS
            Retrieves Hard Drive Smart information from the ESX host.

        .PARAMETER DriveExclusions
            Script will pick up every drive attached to ESX host. Including USB drives.
            See notes to work out any unwanted drive names.

        .EXAMPLE
            Get-SmartData

        .NOTES
            Author: Graham Jordan

            To determine your unwanted drives, run:
            Get-VMHost | Where-Object { $_.ConnectionState -eq 'Connected' } | Get-ScsiLun | Select CanonicalName, Model
    
    #>

    [cmdletbinding()]

    Param( 

        [Parameter( Mandatory = $false )]
        [array]$DriveExclusions = @( "Ultra Fit" )

    )

    # Gets list of all ESX hosts.
    $esx = Get-VMHost | Where-Object { $_.ConnectionState -eq 'Connected' }

    [Collections.ArrayList]$driveData = @()
    $smartData = @()

    if ( !( $esx ) ){
        throw "No valid connection to a VMWare host(s) found. Exiting script." 
    }

    # Gets a list of all hard drives within the ESXHost(s).
    foreach ( $esxServer in $esx ){
        $driveData += $esxServer | Get-ScsiLun
    }

    if ( $driveData.Count -lt 1 -or $null -eq $driveData ){
        throw "No drives found on the VMware host(s)."
    }

    # Any items found in the drive exclusion list, remove from array.
    foreach ( $driveExclusion in $DriveExclusions ){
        $removePoint = $driveData.Model.IndexOf( $driveExclusion )
        $driveData.RemoveAt( $removePoint )
    }

    foreach ( $esxServer in $esx ){

        $bootTime = Get-Date ( $esx.ExtensionData.Summary.RunTime.BootTime )

        # Not really sure. : https://vlenzker.net/2016/06/get-hyperconverged-smart-information-via-powercli/
        $esxcli = $esxServer | Get-EsxCli -V2

        $arguments = $esxcli.storage.core.device.smart.get.CreateArgs()

        # For each hard drive, extract smart data.
        foreach ( $drive in $driveData ){

            # Devicename has to be in lowecase.
            $arguments.devicename = $drive.canonicalName

            $smart = $esxcli.storage.core.device.smart.get.Invoke( $arguments )
            $temp = ( $smart | Where-Object { $_.Parameter -contains 'Drive Temperature' } ).Value

            if ( !( $temp ) ){
                throw "Could not ascertain temperature of '$( $drive.Name )'."
            }

            # Converts the temperature found on SSD's.
            if ( $drive.IsSsd -eq $true -and $drive.Vendor -ne "NVMe" ){
                [int]$temp = [math]::Round( ( $temp -32 ) / 1.8 )
            }

            if ( $drive.IsSsd -eq $true ){
                [int]$driveSSD = 1
                [int]$driveTempLow = 0
                [int]$driveTempHigh = 70
            } else {
                [int]$driveSSD = 0
                [int]$driveTempLow = 25
                [int]$driveTempHigh = 45
            }

            # Compiles drive data into an object.
            $smartdata += [PSCustomObject]@{
                Host                = $ESXServer.Name
                DriveName           = $drive.canonicalName
                Capacity            = [math]::Round( $drive.CapacityGB, 2 )
                IsSSD               = $driveSSD
                Model               = $drive.Model
                Health              = ( $smart | Where-Object { $_.Parameter -contains 'Health Status' } ).Value
                WriteError          = ( $smart | Where-Object { $_.Parameter -contains 'Write Error Count' } ).Value
                WriteErrorThreshold = ( $smart | Where-Object { $_.Parameter -contains 'Write Error Count' } ).Threshold
                WriteErrorWorst     = ( $smart | Where-Object { $_.Parameter -contains 'Write Error Count' } ).Worst
                ReadError           = ( $smart | Where-Object { $_.Parameter -like 'Read Error Count' } ).Value
                ReadErrorThreshold  = ( $smart | Where-Object { $_.Parameter -like 'Read Error Count' } ).Threshold
                ReadErrorWorst      = ( $smart | Where-Object { $_.Parameter -like 'Read Error Count' } ).Worst
                PowerCycleCount     = ( $smart | Where-Object { $_.Parameter -like 'Power Cycle Count' } ).Value
                ReallocatedSectors  = ( $smart | Where-Object { $_.Parameter -like 'Reallocated Sector Count' } ).Value
                RawReadErrorRate    = ( $smart | Where-Object { $_.Parameter -like 'Raw Read Error Rate' } ).Value
                Temperature         = $temp
                DriveTempLow        = $driveTempLow
                DriveTempHigh       = $driveTempHigh
                Date                = $( Get-Date -Format "yyyy-MM-dd HH:mm:ss" )
                DriveUpTime         = [math]::Round( ( ( ( Get-Date ) - $bootTime ).TotalHours ), 2 )
            }
        }
    }

    Return $smartData

}
