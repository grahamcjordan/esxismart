
function Add-ToSmartDB {

    <#

        .SYNOPSIS
            Adds Extended Smart Data to SQL Database.

        .PARAMETER SQLConnection
            [MANDATORY] Connection to SQL.

        .PARAMETER SQLDatabaseName
            Name of SQL database.

        .PARAMETER ExtendedSmartData
            [MANDATORY] PowerShell Object Array containing Smart and Disk data of each drive on ESXi.

        .NOTES
            Author: Graham Jordan

    #>

    [cmdletbinding()]

    Param (

        [Parameter( Mandatory = $true )]
        [System.Data.Common.DbConnection]$SQLConnection,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLDatabaseName = "ESXiDrives",

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$ExtendedSmartData

    )

    # If connection to SQL database isn't open, open.
    if ( $SQLConnection.State -ne "Open" ){
        try {
            $SQLConnection.Open()
            Write-Output "Opened connection to $( $SQLConnection.DataSource )."
        } catch {
            throw "Cannot connect to $( $SQLConnection.DataSource ). Error:$( $_.Exception.Message )"
        }
    }

    $sql = New-Object MySql.Data.MySqlClient.MySqlCommand
    $sql.Connection = $SQLConnection

    $sql.CommandText = "USE $SQLDatabaseName"
    $Sql.ExecuteNonQuery() | Out-Null

    foreach ( $drive in $ExtendedSmartData ){

        $driveCanonicalName = $drive.'Drive CanonicalName'
        $Sql.CommandText = "SELECT DriveInfo.DiDriveId FROM `DriveInfo` WHERE DriveInfo.Canonical_Name = '$driveCanonicalName'"

        # execute SQL search command
        $reader = $sql.ExecuteReader()

        while( $reader.Read() ){ 
            $driveId = $reader.GetString( 0 ) 
        }

        $reader.Close()

        if ( !$driveId ){
            Write-Error "Could not determine the value of 'DiDriveId' for $driveCanonicalName. Cannot import data into SQL."
        } else {
            try {
                $driveName                = $drive.'Drive CanonicalName'
                $driveHealth              = $drive.Health
                $driveReadError           = $drive.ReadError
                $driveReadErrorThreshold  = $drive.ReadErrorThreshold
                $driveReadErrorWorst      = $drive.ReadErrorWorst   
                $driveRawReadErrorRate    = $drive.RawRedErrorRate     
                $driveWriteError          = $drive.WriteError
                $driveWriteErrorThreshold = $drive.WriteErrorThreshold
                $driveWriteErrorWorst     = $drive.WriteErrorWorst
                $drivePowerCycleCount     = $drive.PowerCycleCount
                $driveReallocatedSectors  = $drive.ReallocatedSectors
                $driveTemp                = $drive.Temperature
                $driveDate                = $drive.Date
                $driveUpTimeInHours       = $drive.'Drive UpTime'

                $sql.CommandText = "INSERT INTO DriveStats 
                    ( DiDriveId,Health,ReadError,ReadErrorThreshold,ReadErrorWorst,WriteError,WriteErrorThreshold,WriteErrorWorst,
                    PowerCycleCount,ReallocatedSectorCount,RawReadErrorRate,Temperature,Date,DriveUpTimeinHours ) 
                    VALUES
                    ( '$driveID','$driveHealth', '$driveReadError', '$driveReadErrorThreshold', '$driveReadErrorWorst',
                        '$driveWriteError', '$driveWriteErrorThreshold', '$driveWriteErrorWorst', '$drivePowerCycleCount',
                        '$driveReallocatedSectors', '$driveRawReadErrorRate', '$driveTemp', '$driveDate', '$driveUpTimeInHours' )"

                $sql.ExecuteNonQuery() | Out-Null

                Write-Output "Inserted SMART drive data for '$driveName'"
            } catch {
                Write-Error "Failed to import data into SQL for '$driveName'. `nError: $( $_.Exception.Message )"
            }
        }
    }
}
