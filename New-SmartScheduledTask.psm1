
function New-SmartScheduledTask {

    <#
    
        .SYNOPSIS
            Creates a Windows Scheduled Task by writing a ps1 file, and calling it every 30 minutes.

        .DESCRIPTION
            Uses parameters from the main script to generate a ps1 file in the same location the script is ran fom.
            The file will look something like this:

                Import-Module C:\Users\Graham\SMART\ESXiSMART.psd1 -Force -Global -ErrorAction SilentlyContinue

                $decodingSQL = Import-Clixml -Path "C:\Users\Graham\SMART\SQLCredentials.Credential"
                $sqlPass     = $decodingSQL.GetNetworkCredential().SecurePassword

                $decodingESXi = Import-Clixml -Path "C:\Users\Graham\SMART\ESXiCredentials.Credential"
                $esxiPass     = $decodingESXi.GetNetworkCredential().SecurePassword

                [hashtable]$startSmart = @{
                    ESXiPass        = $esxiPass
                    ESXiHost        = "192.168.0.5"
                    SQLDatabaseName = "ESXiDrives"
                    SQLUser         = "root"
                    SQLPass         = $sqlPass
                    SQLPort         = 3306
                    SQLHost         = "127.0.0.1"
                }
 
                $startSmart.Add( "Email", "grahamcjordan@contoso.com" )
                $startSmart.Add( "EmailPassPath", "C:\Users\Graham\SMART\grahamcjordancontoso.credential" )

                Start-SmartDataScript @startSmart -Verbose

            The script will then create a scheduled task called "SMart Data Grab" to first run at 3am, then, 
            every 30 minutes thereafter.

        .PARAMETER ESXiUser
            Username for ESXi. Default is Root.

        .PARAMETER ESXiPass
            [MANDATORY] Password for ESXiUser. In securestring format. If running function from a prompt, must use:
            Start-SmartDataScript -ESXiPass $( "MyESXiPass!" | ConvertTo-SecureString -AsplainText -Force )

        .PARAMETER ESXiHost
            [MANDATORY] IP or hostname for ESXi host.

        .PARAMETER SQLDatabaseName
            Name of SQL database. Default is ESXiDrives

        .PARAMETER SQLUser
            Username for MySQL. Defaults to Root.

        .PARAMETER SQLPass
            [MANDATORY] Password for SQLUser. In securestring format. If running function from a prompt, must use:
            Start-SmartDataScript -SQLPass $( "MySQLPass!" | ConvertTo-SecureString -AsplainText -Force )

        .PARAMETER SQLHost
            IP or hostname for computer with MySQL installed.

        .PARAMETER SQLPort
            Port to connect MySQL server. Defaults to 3306.

        .PARAMETER Email
            Email address you're both sending to and authentication from.

        .PARAMETER EMailPassPath
            Folder Path to cliXml file containing email credentials.

        .NOTES
            Author Graham Jordan
            Version 1
    
    #>

    [cmdletbinding()]

    Param (

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$ESXiUser = 'root',

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [securestring]$ESXiPass,

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [string]$ESXiHost,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLDatabaseName = 'ESXiDrives',

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLUser = 'root',

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [securestring]$SQLPass,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLHost = '127.0.0.1',

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [int]$SQLPort = 3306,

        [Parameter( Mandatory = $false )]
        # Simple regex to capture _most_ email addresses. https://stackoverflow.com/a/719543/5681924
        [ValidateScript({ $_ -match "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" })]
        [mailaddress]$Email,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$EmailPassPath

    )

    # Credential objects
    $sqlCreds  = New-Object -TypeName pscredential( $SQLUser, $SQLPass )
    $esxiCreds = New-Object -TypeName pscredential( $ESXiUser, $ESXiPass )

    $saveLocation = ( Get-Location ).Path

    # Export credentials securely
    try {
        $sqlCreds | Export-Clixml -Path "$saveLocation\SQLCredentials.Credential" -Force
        $esxiCreds | Export-Clixml -Path "$saveLocation\ESXiCredentials.Credential" -Force
    } catch {

        try {
            # Found on my own machine, Windows Defender locks my folder for any potential harmful files. So allow PowerShell 
            # through, and exclude the folder. We'll reset this later. Works when running as admin. Which you have to be fpr ESXi.
            Add-MpPreference -ControlledFolderAccessAllowedApplications 'c:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe' -Verbose
            Start-Sleep -Seconds 5
            Add-MpPreference -ExclusionPath $saveLocation -Force -Verbose
            Start-Sleep -Seconds 5
            $alteredMpPreference = $true

            $sqlCreds | Export-Clixml -Path "$saveLocation\SQLCredentials.Credential" -Force
            $esxiCreds | Export-Clixml -Path "$saveLocation\ESXiCredentials.Credential" -Force
        } catch {
            Write-Warning -Message "Unable to write files to '$( $saveLocation
                )'. This is because Windows Defender has locked the folder. Writing to '$( $env:TEMP 
                )' instead. From experience, a Windows Hotfix will empty the temp folder, meaning $(  
                )the scheduled task will stop working when said updates are applied and exported files deleted."
            try {
                # Writes out credentials to temp folder if cannot write to original location,
                $sqlCreds | Export-Clixml -Path "$env:TEMP\SQLCredentials.Credential" -Force
                $esxiCreds | Export-Clixml -Path "$env:TEMP\ESXiCredentials.Credential" -Force
            } catch {
                Write-Warning -Message "Cannot write to temp folder either. Will have to use plain text passwords. Ouch."
            }
        }
    }

    # If credential file was exported to same location as script runs from, then set $decodingPasswords to Import CLiMXL 
    # from said area. This will keep the passwords hashed out in the scheduledTask.sp1 script. 
    if ( ( Test-Path -Path "$saveLocation\SQLCredentials.Credential" ) -eq $true ){

        $decodingPasswords = @"

            `$decodingSQL = Import-Clixml -Path "$( $saveLocation )\SQLCredentials.Credential"
            `$sqlPass     = `$decodingSQL.GetNetworkCredential().SecurePassword

            `$decodingESXi = Import-Clixml -Path "$( $saveLocation )\ESXiCredentials.Credential"
            `$esxiPass     = `$decodingESXi.GetNetworkCredential().SecurePassword
"@
    # As with above, this will prevent any hardcoded passwords in any script.
    } elseif ( ( Test-Path -Path "$env:TEMP\SQLCredentials.Credential" ) -eq $true ){

        $decodingPasswords = @"

            `$decodingSQL = Import-Clixml -Path "$( $env:TEMP )\SQLCredentials.Credential"
            `$sqlPass     = `$decodingSQL.GetNetworkCredential().SecurePassword

            `$decodingESXi = Import-Clixml -Path "$( $env:TEMP )\ESXiCredentials.Credential"
            `$esxiPass     = `$decodingESXi.GetNetworkCredential().SecurePassword
"@
    # When credential files cannot be exported, will hardcode password into scheduled Task file.
    } else {
        $sqlPassInsecure  = $sqlCreds.GetNetworkCredential().Password
        $esxiPassInsecure = $esxiCreds.GetNetworkCredential().Password  

        $decodingPasswords = @"

            `$esxiPassInsecure = `"$esxiPassInsecure`"
            `$esxiPass = $( $esxiPassInsecure | ConvertTo-SecureString -AsPlainText -Force )

            `$sqlPassInsecure = `"$sqlPassInsecure`"
            `$sqlPass = $( $sqlPassInsecure | ConvertTo-SecureString -AsPlainText -Force )
"@
    }

    # If EmailPassPath variable specified, then add to the scheduled task file output.
    if ( $EmailPassPath ){
        $addEmail = @"

        `$startSmart.Add( "Email", `"$Email`" )
        `$startSmart.Add( "EmailPassPath", `"$EmailPassPath`" )

"@
    } else {
        $addEmail = " "
    }

    # Create a scheduled task string to be outputted.
    $scheduledTask = @"

        Import-Module $( $saveLocation )\ESXiSMART.psd1 -Force -Global -ErrorAction SilentlyContinue

        $decodingPasswords

        [hashtable]`$startSmart = @{
            ESXiPass        = `$esxiPass
            ESXiHost        = `"$ESXiHost`"
            SQLDatabaseName = `"$SqlDatabaseName`"
            SQLUser         = `"$SQLUser`"
            SQLPass         = `$sqlPass
            SQLPort         = $SQLPort
            SQLHost         = `"$SQlHost`"
        }

        $addEmail

        Start-SmartDataScript @startSmart -Verbose

"@

    # Exports scheduled task file, ideally to the same location the module resides in, but will use the temp folder as a fallback.
    try {
        $scheduledTask | Out-File -FilePath "$saveLocation\ScheduledTask.ps1" -Encoding Ascii -Force
        $schedTaskPath = "$saveLocation\ScheduledTask.ps1"
    } catch {
        try {
            $scheduledTask | Out-File -FilePath "$env:TEMP\ScheduledTask.ps1" -Encoding Ascii -Force
            $schedTaskPath = "$env:TEMP\ScheduledTask.ps1"

            Write-Warning -Message ( "Unable to write files to '$saveLocation'. This is because Windows Defender has " +
                "locked the folder. Writing to '$env:TEMP' instead. From experience, a Windows Hotfix will empty the " +
                "temp folder, meaning the scheduled task will stop working when said updates are applied and exported " +
                "files deleted." )
        } catch {
            Write-Error "Unable to export newly generated Schedule Task file. Error is $( $_.Exception.Message 
                ). Cannot create scheduled task."
            Start-Sleep Seconds 5 
            Exit
        }
    }

    # Scheduled Task actions. Run PowerShell with an argument of the path to the newly created file.
    $action = @{
        Execute  = 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe'
        Argument = $schedTaskPath
    }

    # Scheduled Task triggers. On scheduled tasks, this is the Triggers screen.
    $trigger = @{
        Once     = $true
        At       = '3am'
        RepetitionInterval = [timespan]::FromMinutes( 30 )
        RepetitionDuration = [timespan]::FromDays( 3000 ) #( [timeSpan]::MaxValue )<--- This should work. It doesn't. 8 years should be enough, surely?
    }

    # Scheduled Task Settings. Configures the options for the scheduled task.
    $settings = @{
        RestartCount               = 3
        RestartInterval            = [timespan]::FromMinutes( 1 )
        ExecutionTimeLimit         = [timespan]::FromHours( 1 )
        MultipleInstances          = 'Parallel'
        AllowStartIfOnBatteries    = $true 
        DontStopIfGoingOnBatteries = $true
    }

    # Builds all the scheduled task configurable options.
    $stAction     = New-ScheduledTaskAction @action
    $stTrigger    = New-ScheduledTaskTrigger @trigger
    $stSettings   = New-ScheduledTaskSettingsSet @settings
    $stPrinicipal = New-ScheduledTaskPrincipal -RunLevel Highest -UserId "$env:COMPUTERNAME\$env:USERNAME" -LogonType S4U

    # hashtable for activating the scheduled task.
    $registerScheduledTask = @{
        Action      = $stAction
        Trigger     = $stTrigger
        Settings    = $stSettings
        TaskName    = "SMART Data Grab"
        Description = "SMART Data Grab"
        TaskPath    = "\"
        Principal   = $stPrinicipal
        Force       = $true
    }

    # Activates the scheduled task with the hash table above.
    Register-ScheduledTask @registerScheduledTask -Verbose

    # If Windows Defender was hacked, undo the changes
    if ( $alteredMpPreference ){
        Remove-MpPreference -ExclusionPath $saveLocation -Force
        Remove-MpPreference -ControlledFolderAccessAllowedApplications 'c:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe' 
    }

}