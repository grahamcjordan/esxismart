# README #

    Extract SMART Data from VMWare ESXi and import into a MySQL database.

### Version 3.0.0

    - Database Structure
        - ConfigSettings is altered at the website level. If ConfigSettings has not changed, then PowerShell will not run
          a schema update. If however the website is at version 3 also, the PowerShell script will add a new table called
          ESXiData, which holds just a single row of hardware data that's overwritten upon each run.

    - Scheduled Task Creation
        - If Windows OS, generates a scheduled task for you. Will attempt to encrypt passwords also so any files lingering 
          around will not have plain text passwords to your MySql, ESXi and email.

    - Linux Support
        - Tested on Ubuntu.

    - Other Enhancements
        - Refer to changelog.md

### Version 2.0.0 Enhancements

    - Database Sctructure
        - DriveInfo has three new columns. DriveTempHigh    DriveTempLow    IsPresent
          These are added within the Initialize-SQLSmartDatabase function.
          If your website is still running version 1, it will not affect the end website.

    - Email alerts
        - If (not necessary) an email address is provided at the point of running the script, checks will take place to 
          send out alerts when SMART data thresholds are breeched.
          With this in mind, it would make sense to run this script once from an elevated PowerShell window so your email
          password can be provided. Use the following example to do this.

          Start-SmartDataScript -Email myemail@mydomain.com -EmailPass myEmailPassword

          At this point a credential file will be generated in your temp folder called myemailmydomain.credential
          This file is encrypted, and can only be unencrypted by you, on the machine in which the script is ran.
          Any encryption method that requires decrypting credentials isn't perfect. This is the best one could come up with.
          https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/export-clixml?view=powershell-7.1

    - 2 Factor Authentication
          If you're using 2-Factor Authentication with your email provider (and if you're not, you really should! For 
          EVERYTHING) then your standard password will not work. As this script has no way of handling 2-Factor authentication
          you will need an application password.

          For Outlook / Hotmail / msn addresses, use this website for guidance. 
          https://support.microsoft.com/en-us/account-billing/using-app-passwords-with-apps-that-don-t-support-two-step-verification-5896ed9b-4263-e681-128a-a6f2979a7944#:~:text=How%20to%20create%20a%20new%20app%20password%201,your%20normal%20Microsoft%20account%20password%20in%20the%20application.

          For Gmail use this website
          https://support.google.com/accounts/answer/185833?hl=en

          For Yahoo use:
          https://my.help.yahoo.com/kb/account/generate-third-party-passwords-sln15241.html?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuYmluZy5jb20v&guce_referrer_sig=AQAAAGkH4Bv6G4DbYwzQc1g-vRXgxH8T2lEvT_uVSPrwGwioaDMUXzLwH1a08_I2ByA-MWJb9jUztzbL8KboaH-BthdXhcmP-vrwxDN1ghUc07emLMHziOmYcbvyDzJBW3gfkZiW-Bn2JMZAXJ5Q-qxhSVrB9-SyXs1KoiHxv_Uy3Z_u


### Version 1.0.0

### PreRequisites

    Computer running PowerShell version 5+
        If a windows device, you must alter the execution policy. To do this, open PowerShell as an administrator
        and type 'Set-ExecutionPolicy -ExecutionPolicy Unrestricted'

    MySQL
        
        XAMPP
            The default settings of the script have XAMPP users in mind.
        Synology
            If you're hosting on a Synology box with MariaDB10 you'll need to add the parameter -SQLPort 3307 when 
            running the script.
            You must also create a new user account as root by default does not allow remote connections. 
            Use phpmyadmin to create a new user, making sure host 'Host name' is set to Any host.
            The command to create a new user is:
            CREATE USER 'SMART'@'%' IDENTIFIED BY 'ChangeMeToSomethingSecure12!' GRANT ALL PRIVILEGES ON *.* TO 'SMART'@'%' 
            REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 
            MAX_USER_CONNECTIONS 0;

    MySQL.dll
        The script will attempt to detect MySQL.dll, if not found it'll prompt you to download from:
        https://dev.mysql.com/downloads/connector/net/.

### Database Sctructure

    ESXiDrives
        DriveInfo
            DiDriveId   VM_Host   Drive_Host   Drive_Controller   Drive_Number   Canonical_Name   Model   RDM_Name   SSD   Capacity

        DriveInfo will only update when a new drive is detected. Removed drives will not be deleted.
        DiDriveId is the foreign key.
        VM_Host             : ESXHost
        Drive_Host          : VMWare Virtual Machine OS Name
        Drive_Controller    : Virtual Machine Hard Drive Controller number
        Drive_Number        : Virtual Machine Hard Drive number
        Canonical_Name      : VMWare Canonical_Name
        Model               : Hard Drive Model Name
        RDM_Name            : Path of Raw Device Mapping File
        SSD                 : Bool on SSD Device
        Capacity            : Capacity of Hard Drive in GB

        DriveStats
            DiDriveId   Health   ReadError   ReadErrorThreshold   ReadErrorWorst   WriteError   WriteErrorThreshold   WriteErrorThreshold   WriteErrorWorst PowerCycleCount   ReallocationSectorCount   RawReadErrorRate   Temperature   Date   DriveUpinHours

        All fields here pulled directly from the SMART script, with the exception of Date and DriveUpinHours, which are both calculated variables.
        Temperature is in Celcius.

### How to use as a scheduled task on Windows 10

    Note repo download location (for purpose of README.md we'll use C:\Users\Graham\Documents\esxismart)

    Create a new PowerShell file in any location (for purpose of README.md we'll use C:\Users\Graham\Documents\ESXiScheduledTask.ps1 ) with the following line:
    
        Import-Module C:\Users\Graham\Documents\esxismart\ESXiSMART.psd1 -Force -Global -ErrorAction SilentlyContinue

    Now copy the following hashtable into the script, updating parameters as necessary:

        [hashtable]$startSMART = @{
            SQLPass  = ChangeMeToSomethingSecure12!
            ESXiPass = ThisShouldAlreadyBeSecure123!
            ESXiHost = "192.168.0.5"
        }

    If you're database is going to be hosted on Synology, extend the hashtable with:

            SQLUser = SMART
            SQLPort = 3307
            SQLHost = SynologyIP_or_DNSName

    Then the last line of the PowerShell script should be:

    Start-SmartDataScript @startSMART

    On my setup, my scheduled task looks like this (with passwords changed for obvious reasons!)

        Import-Module C:\Users\Graham\Documents\Repos\esxismart\ESXiSMART.psd1 -Force -Global -ErrorAction SilentlyContinue

        [hashtable]$startSmart = @{
            ESXiPass = ESXiPass123!
            ESXiHost = "192.168.0.5"
            SQLUser  = "HardDrives"
            SQLPort  = 3307 
            SQLPass  = SQLPassword123!
            SQLHost  = "192.168.0.190"
        }

        Start-SmartDataScript @startSMART

    Finally, when starting your Scheduled Task, your Actions tab should be:

    Action:                     Start a program
    Program/script:             PowerShell
    Add arguments (optional):   "C:\Users\Graham\Documents\ESXiScheduledTask.ps1"
    Start in (optional):        "C:\Users\Graham\Documents\esxismart"
