
function Start-AlertChecks {

    <#

        .SYNOPSIS
            Performs alert checks. 

        .DESCRIPTION
            Compares data from recent SMART capture against alert thresholds and sends emails if necessary.

        .PARAMETER SQLConnection
            [MANDATORY] SQL Connection. Will open a new connection if closed already.

        .PARAMETER SQLDatabaseName
            Name of Database holding ESXi SMART information.

        .PARAMETER ExtendedSmartData
            [MANDATORY] Array containing complete details for a hard drive.

        .PARAMETER Email
            [MANDATORY] Email address you're both sending to and authentication from.

        .PARAMETER EMailPassPath
            [MANDATORY] If running from a Windows Machine, this is a scrambled/encrypted file containing your
            email password. This can only be opened by you, on this machine. Any other user or any other
            computer and this cannot be decrypted.

        .PARAMETER HTMLTemplate
            Path to HTML Template file used to form the base email.

        .PARAMETER MailPort
            Microsoft, Gmail and Yahoo all use port 587 to send email. If a personal mail provider uses a different port
            then provide it here.

        .NOTES
            Email, EMailPassPath and MailProvider are passedthrough to Send-AlertEmail should it be necessary.

    #>

    [cmdletbinding()]

    param(

        [Parameter( Mandatory = $true )]
        [System.Data.Common.DbConnection]$SQLConnection,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLDatabaseName = "ESXiDrives",

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$ExtendedSmartData,

        [Parameter( Mandatory = $true )]
        # Simple regex to capture _most_ email addresses. https://stackoverflow.com/a/719543/5681924
        [ValidateScript({ $_ -match "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" })]
        [mailaddress]$Email,

        [Parameter( Mandatory = $true )]
        [ValidateScript({ Test-Path -Path $_ })]
        [string]$EmailPassPath,

        [Parameter( Mandatory = $false )]
        [ValidateScript({ Test-Path -Path $_ })]
        [string]$HTMLTemplate = "$( $PSScriptRoot )\template.html",

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [int]$MailPort = 587

    )

    # If connection to SQL database isn't open, open.
    if ( $SQLConnection.State -ne "Open" ){
        try {
            $SQLConnection.Open()
            Write-Output "Opened connection to $( $SQLConnection.DataSource )."
        } catch {
            Write-Error "Cannot connect to $( $SQLConnection.DataSource ). `nError:$( $_.Exception.Message )"
            Start-Sleep -Seconds 10 
            throw
        }
    }

    Write-Output "Collecting data from 'DriveInfo'"

    # Collect data from DriveInfo database.
    try {
        $sql = New-Object MySql.Data.MySqlClient.MySqlCommand
        $sqlDataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter
        $sqlDataSet = New-Object System.Data.DataSet
        $sql.Connection = $sqlConnection

        $sql.CommandText = "USE $SQLDatabaseName"
        $sql.ExecuteNonQuery() | Out-Null

        $sql.CommandText = 'SELECT * from DriveInfo'
        $sqlDataAdapter.SelectCommand = $sql
        $sqlRowCount = $sqlDataAdapter.Fill( $sqlDataSet, "data" )
    } catch {
        throw "Unable to collect data from SQL, specifically 'DriveInfo' from '$SQlDatabaaseName'. `nError: $(
            $_.Exception.Message )"
    }

    foreach ( $drive in $ExtendedSmartData ){

        Write-Output "Checking $( $drive.'Drive CanonicalName' ) for errors"
        $errors = $null

        # SQL data for specific drive found in ExtendedData
        $sqlDriveData = $sqlDataSet.tables[0] | Where-Object { $_.Canonical_Name -eq $drive.'Drive CanonicalName' }

        # table containing details of drive of which will be translated into an email.
        [hashtable]$errorFound = @{
            'SQL Drive ID'         = $sqlDriveData.DiDriveId
            'Canonical Name'       = $drive.'Drive CanonicalName'
            'Drive Health'         = $drive.Health
            Capacity               = $drive.'Drive Capacity'
            Temperature            = $drive.Temperature
            'Guest Machine'        = $drive.'Drive Host'
            'Controller Location'  = $drive.'Controller Location'
            'VM Host Drive Number' = $drive.'Drive Number'
        }

        # Checks if the drive temp is outside of the threshold
        if ( [int]$drive.Temperature -gt [int]$sqlDriveData.DriveTempHigh -or [int]$drive.Temperature -lt [int]$sqlDriveData.DriveTempLow ){

            $errorFound.Add( 'Subject', 'Drive Temperature threashold breeched' )
            $errorFound.Add( 'Error' , "Drive Temperature threshold breeched. <br />Temperature is $( 
                $drive.Temperature ) degrees celsius." )

            $errors = [PSCustomObject]$errorFound
        }
    
        # If SMART data has recorded a status other than OK, create a slighty altered table for an email.
        if ( $drive.Health -ne "OK" ){

            $errorFound.Add( "Subject", "Status of Drive has changed" )
            $errorFound.Add( "Error", "SMART health status on $( $drive.'Drive CanonicalName' 
                ) is no longer 'OK'. <br />Reporting as $( $drive.Health )." )
            $errorFound.Add( "RDM Path", $drive.'RDM File Name' )
            $errorFound.Add( "Write Error", $drive.WriteError )
            $errorFound.Add( "Write Error Threshold", $drive.WriteErrorThreshold )
            $errorFound.Add( "Write Error Worst", $drive.WriteErrorWorst )
            $errorFound.Add( "Read Error", $drive.ReadError )
            $errorFound.Add( "Read Error Threshold", $drive.ReadErrorThreshold )
            $errorFound.Add( "Read Error Worst", $drive.ReadErrorWorst )
            $errorFound.Add( "Power Cycle Count", $drive.PowerCycleCount )
            $errorFound.Add( "Reallocated Sectors", $drive.ReallocatedSectors )
            $errorFound.Add( "Raw Read Error Rate", $drive.RawReadErrorRate )

            $errors = [PSCustomObject]$errorFound
        }

        # If an error is found, pass through errors to Send-AlertEmail function.
        if ( $errors ){
            [hashtable]$sendEmailError = @{
                Errors        = $errors
                Email         = $Email
                EmailPassPath = $EmailPassPath
                HTMLTemplate  = $HTMLTemplate
                MailPort      = $MailPort
            }

            Write-Warning "An error has been detected on $( $drive.'Drive CanonicalName' ). Sending an email alert."
            Send-AlertEmail @sendEmailError
        }
    }

    # Check to see if any drives are missing by comparing drives found in ExtendedSmartData and those found in DB.
    foreach ( $drive in $sqlDataSet.tables[0] ){
        if ( $extendedSmartData.'Drive CanonicalName' -notcontains $drive.'Canonical_Name' ){
            # If a drive is missing from SMART data rip...
            $errors            = $null
            $sqlNew            = New-Object MySql.Data.MySqlClient.MySqlCommand
            $sqlDataAdapter2   = New-Object MySql.Data.MySqlClient.MySqlDataAdapter
            $sqlDataSet2       = New-Object System.Data.DataSet
            $sqlNew.Connection = $sqlConnection

            #... Run an SQL command to get the last entry into DriveStats for said drive.
            $sqlNew.CommandText = "SELECT DriveStats.* 
                FROM DriveStats INNER JOIN DriveInfo ON DriveInfo.DiDriveId = DriveStats.DiDriveId 
                WHERE DriveInfo.IsPresent = 0 AND DriveInfo.Canonical_Name = '$( $drive.'Canonical_Name' )'
                ORDER BY Date DESC LIMIT 1"
            $sqlDataAdapter2.SelectCommand = $sqlNew
            $sqlRowCount2 = $sqlDataAdapter2.Fill( $sqlDataSet2, "data" )
            $sqlDriveData2 = $sqlDataSet2.tables[0]

            # Now check if the disappeared less than a day ago. If so, then send an alert to say removed.
            if ( ( Get-Date $sqlDriveData2.Date ).AddDays( +1 ) -ge ( Get-Date ) ){

                Write-Warning "$( $drive.'Canonical_Name' ) is missing. Sending alert."

                # Records how long the alerts will keep up for. 24 hours is the chosen value.
                $alertTimeSpan = New-TimeSpan -Start ( Get-Date ) -End ( Get-Date $sqlDriveData2.Date ).AddDays( +1 )

                [string]$eMessage = ( "Drive: <strong>$( $drive.Canonical_Name )</strong> is missing from ESXi. <br />Last " +
                    "'good' status check-in was at '$( Get-Date $sqlDriveData2.Date -Format F )'. <br />This alert will " +
                    " continue for $( $alertTimeSpan.Hours ) hours and $( $alertTimeSpan.Minutes ) minutes." )

                [hashtable]$errorFound = @{
                    Subject                = "Drive No Longer Present in ESXi"
                    Error                  = $eMessage
                    'Canonical Name'       = $drive.Canonical_Name
                    Capacity               = $drive.Capacity
                    'RDM File Name'        = $drive.RDM_Name
                    'SQL Drive ID'         = $drive.DiDriveId
                    'Guest Machine'        = $drive.Drive_Host
                    'Controller Location'  = $drive.Drive_Controller
                    'VM Host Drive Number' = $drive.Drive_Number
                }
    
                $errors = [PSCustomObject]$errorFound
                
                [hashtable]$sendEmailError = @{
                    Errors        = $errors
                    Email         = $Email
                    EmailPassPath = $EmailPassPath
                    HTMLTemplate  = $HTMLTemplate
                    MailPort      = $MailPort
                }

                Send-AlertEmail @sendEmailError
            }
        }
    }
}
