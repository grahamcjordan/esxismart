
function Initialize-SQLSmartDatabase {

    <#
    
        .SYNOPSIS
            Builds an SQL Database for SMART data

        .PARAMETER SQLConnection
            [MANDATORY] Requires an SQL connection. Should be in the form of:
                $sqlConnection = [MySql.Data.MySqlClient.MySqlConnection]@{
                    ConnectionString="server=$SqlHost;port=$SQLPort;uid=$SQLUser;pwd=$SQLPass;SslMode=none"}

        .PARAMETER SQLDatabaseName
            Name of SQL Database. Default is ESXiDrives

        .PARAMETER ExtendedSmartData
            [MANDATORY] PowerShell Object Array containing drive data.

        .NOTES
            Author: Graham Jordan

            ## VersionNo
                In version 3 of the website, a schema update takes place on ConfigSettings creating two new columns. 
                They are, WebVersionNo and PsVersionNo.
                The website will automatically set PsVersionNo to 2.
                There are zero benefits to upgrading the database to version 3 within ESXISMART unless the website is 
                updated. Unlike the upgrade from 1 to 2 where either the website or ESXISMART could perform the db 
                upgrade, this PS script will only update the schema if ConfigSettings > WebVersionNo is 3+. 
                This is the same for a fresh install, the PS script will assume the role of version 2 until it recognises 
                the website as version 3.
    
    #>

    [cmdletbinding()]

    param(

        [Parameter( Mandatory = $true )]
        [System.Data.Common.DbConnection]$SQLConnection,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLDatabaseName = "ESXiDrives",

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$ExtendedSmartData

    )

    # Opens SQL connection if not open already.
    if ( $SQLConnection.State -ne "Open" ){
        try {
            $SQLConnection.Open()
            Write-Host "Opened a connection to $( $SQLConnection.DataSource )"
        } catch {
            Write-Error -Message "Unable to open an SQL connection to $( $SQLConnection.DataSource 
                ). `nError: $( $_.Exception.Message )."
            Start-Sleep -Seconds 5
            Exit 
        }
    }

    # Double checks the connection is open. In testing the above does not confirm open, only confirms attempted to open.
    if ( $SQLConnection.State -ne "Open" ){
        Write-Error -Message "Unable to open an SQL connection to $( $SQLConnection.DataSource ). `nCannot Continue"
        Start-Sleep -Seconds 5
        Exit 
    }

    # Searches for existing SQL Database.
    try {
        $sql = New-Object MySql.Data.MySqlClient.MySqlCommand
        $sql.Connection = $SqlConnection

        # Creates an SQL command to search for database then executes.
        $sql.CommandText = "SHOW DATABASES like '$SQLDatabaseName'" 
        $reader = $sql.ExecuteReader()

        while( $reader.Read() ){ 
            $dbName = $reader.GetString( 0 ) 
        }

        $reader.Close()

    } catch {
        Write-Error -Message "Unable to check for existing database '$SQLDatabaseName'. `nError: $( $_.Exception.Message )."
        throw
    }

    if ( !$dbName ){
        Write-Host "Database '$SQLDatabaseName' does not exist. Creating new database."

        # this builds the database and tables
        try {
            $sql.CommandText = "Create DATABASE $SQLDatabaseName;

            Use $SQLDatabaseName;

            CREATE TABLE DriveInfo (
                `DiDriveId` MEDIUMINT(8) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `VM_Host` varchar(255) default NULL,
                `Drive_Host` varchar(255) default NULL,
                `Drive_Controller` varchar(255) default NULL,
                `Drive_Number` varchar(255) default NULL,
                `Canonical_Name` varchar(255) default NULL,
                `Model` varchar(255) default NULL,
                `RDM_Name` varchar(255) default NULL,
                `SSD` TINYINT(1) NOT NULL,
                `Capacity` varchar(100) default NULL,
                `DriveTempHigh` INT(255) NOT NULL,
                `DriveTempLow` INT(255) NOT NULL,
                `IsPresent` TINYINT(1) NOT NULL
            ) AUTO_INCREMENT=1;

            CREATE TABLE DriveStats (
            /*    `DsDriveId` MEDIUMINT(8) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY, */
                `DiDriveId` MEDIUMINT(8) NOT NULL,
                `Health` varchar(100) default NULL,
                `ReadError` varchar(100) default NULL,
                `ReadErrorThreshold` varchar(100) default NULL,
                `ReadErrorWorst` varchar(100) default NULL,
                `WriteError` varchar(100) default NULL,
                `WriteErrorThreshold` varchar(100) default NULL,
                `WriteErrorWorst` varchar(100) default NULL,
                `PowerCycleCount` MEDIUMINT(8) NOT NULL,
                `ReallocatedSectorCount` varchar(100) default NULL,
                `RawReadErrorRate` varchar(100) default NULL,
                `Temperature` MEDIUMINT(8) NOT NULL,
                `Date` datetime default NULL,
                `DriveUpTimeinHours` MEDIUMINT(8) NOT NULL,
                FOREIGN KEY( `DiDriveId` ) REFERENCES DriveInfo( `DiDriveId` )
            );
            "

            $sql.ExecuteNonQuery() | Out-Null
        } catch {
            Write-Error "Unable to create database '$SQLDatabaseName' and/or tables. Error: $( $_.Exception.Message )"
            Start-Sleep -Seconds 5 
            Exit
        }

        # Populates DriveInfo table space with static drive data.
        foreach ( $drive in $ExtendedSmartData ){

            $vmHost          = $drive.Host
            $canonicalName   = $drive.'Drive CanonicalName'
            $driveModel      = $drive.'Drive Model'
            $driveCapacity   = $drive.'Drive Capacity'
            $driveIsSSD      = $drive.SSD
            $driveTempHigh   = $drive.DriveTempHigh
            $driveTempLow    = $drive.DriveTempLow
            $driveHost       = $drive.'Drive Host'
            $driveController = $drive.'Controller Location'
            $driveNumber     = $drive.'Drive Number'
            $driveRDM        = $drive.'RDM File Name'

            try { 
                $sql.CommandText = "INSERT INTO DriveInfo 
                    ( VM_Host,Canonical_Name,Model,Capacity,SSD,Drive_Host,Drive_Controller,Drive_Number,RDM_Name,DriveTempHigh,DriveTempLow,IsPresent ) 
                    VALUES 
                    ( '$vmHost', '$canonicalName', '$driveModel', '$driveCapacity', '$driveIsSSD', '$driveHost',
                        '$driveController', '$driveNumber', '$driveRDM', '$driveTempHigh', '$driveTempLow', 1 )"
                $sql.ExecuteNonQuery() | Out-Null

                Write-Host "Drive information for '$canonicalName' added into database."
            } catch {
                Write-Error -Message "Could not add data for '$canonicalName' into database. `nError: $( 
                    $_.Exception.Message )"
                throw
            }
        }
    } 

    # Default return value of PsVersionNo. 
    # See Notes on version No.
    [int]$psVersionNo = 1

    # Checks database for ConfigSettings. If present, the website has been ran already and all tables are configured correctly.
    $sql.CommandText = "USE $SQLDatabaseName";
    $sql.ExecuteNonQuery() | Out-Null;
    $sql.CommandText = "Select 1 from `ConfigSettings` LIMIT 1"

    try {
        $reader = $sql.ExecuteReader()
        $reader.Close()

        try {
            # SQL command to check PsVersionNo of ConfigSettings. This is only availble when the website is at version 3.0
            # See Notes on Version No. for more information.
            $sql.CommandText = "Select PsVersionNo from ConfigSettings;"
            $reader = $sql.ExecuteReader()

            while( $reader.Read() ){ 
                [int]$psVersionNo = $reader.GetString( 0 ) 
            }

            $reader.Close()

            if ( $psVersionNo -eq 2 ){
                # Creates a new table used for ESXi Usage data and fills with junk fata. Each row will be overwritten on each run of script.
                try {
                    $sql.CommandText = "CREATE TABLE `ESXiData` ( 
                            `MemTotal` VARCHAR(100) NOT NULL, 
                            `MemUsage` VARCHAR(100) NOT NULL, 
                            `CPUTotal` VARCHAR(100) NOT NULL,
                            `CPUUsage` VARCHAR(100) NOT NULL,
                            `DataStoreTotal` VARCHAR(100) NOT NULL,
                            `DataStoreUsage` VARCHAR(100) NOT NULL,
                            `Date` datetime default NULL
                        );

                        INSERT INTO EsxiData (
                            MemTotal, MemUsage, CPUTotal, CPUUsage, DataStoreTotal, DataStoreUsage, Date )
                        VALUES ( '1.0', '1.0', '1.0', '1.0', '1.0', '1.0', '$( $ExtendedSmartData[0].Date )' );
                    "

                    # Executes the above command.
                    $sql.ExecuteNonQuery() | Out-Null
                    
                    # Sets the PsVersionNo table to 3
                    $sql.CommandText = "UPDATE `ConfigSettings` SET `PsVersionNo` = 3;"
                    $sql.ExecuteNonQuery() | Out-Null
                    

                    [int]$psVersionNo = 3
                    Write-Host "Database upgraded to version 3.0"
                } catch {
                    Write-Error "Unable to create ESXiData table. Error: $( $_.Exception.Message )"
                }
            } else {
                [int]$psVersionNo = 3
            }
        } catch {
            $reader.Close()
            [int]$psVersionNo = 2
            Write-Warning "Website is at version 2.0. No need to upgrade database to version 3.0"
        }
    } catch {
        # If ConfigSettings is not present, then add the version 2.0 columns to DriveInfo
        $reader.Close()

        Write-Host "Upgrading database to version 2.0"

        $sql.CommandText = 'ALTER TABLE DriveInfo 
            ADD COLUMN DriveTempHigh INT(255) NOT NULL,
            ADD COLUMN DriveTempLow INT(255) NOT NULL,
            ADD COLUMN IsPresent TINYINT(1) NOT NULL;

            UPDATE DriveInfo SET IsPresent =1  WHERE 1;'

        try {
            $sql.ExecuteNonQuery() | Out-Null
            [int]$psVersionNo = 2
        } catch {
            # If website is not version 2.0 but script already ran, database upgrade completed. Ignore error.
            if ( $_.Exception.Message -match "Duplicate" ){
                Write-Host "Database already upgraded"
            } else {
                Write-Error "Could not alter table DriveInfo with version 2.0 columns. `nError: $( $_.Exception.Message )"
            }
        }
    }

    if ( $dbName ) {
        # If database exists, make sure all drives are up to date and add new ones.
        Write-Host "Database already exists. Checking all drives have entries."

        $sql.CommandText = "USE $SQLDatabaseName"
        $sql.ExecuteNonQuery() | Out-Null

        foreach ( $drive in $ExtendedSmartData ){

            $driveId = $null

            # individual drive data
            $vmHost          = $drive.Host
            $canonicalName   = $drive.'Drive CanonicalName'
            $driveModel      = $drive.'Drive Model'
            $driveCapacity   = $drive.'Drive Capacity'
            $driveIsSSD      = $drive.SSD
            $driveTempHigh   = $drive.DriveTempHigh
            $driveTempLow    = $drive.DriveTempLow
            $driveHost       = $drive.'Drive Host'
            $driveController = $drive.'Controller Location'
            $driveNumber     = $drive.'Drive Number'
            $driveRDM        = $drive.'RDM File Name'

            # Search for drive in database
            try {
                $sql.CommandText = "SELECT DiDriveId FROM `DriveInfo` WHERE Canonical_Name = '$canonicalName'"
                $reader = $sql.ExecuteReader()

                while( $reader.Read() ){ 
                    $driveId = $reader.GetString( 0 )
                }

                $reader.Close()
            } catch {
                "Could not run query $_"
            }

            # if drive missing from database, add.
            if ( !$driveId ){
                Write-Host "Drive $canonicalName is missing from DB. Adding"

                try { 
                    $sql.CommandText = "INSERT INTO DriveInfo 
                        ( VM_Host,Canonical_Name,Model,Capacity,SSD,Drive_Host,Drive_Controller,Drive_Number,RDM_Name,DriveTempHigh,DriveTempLow,IsPresent ) 
                        VALUES 
                        ( '$vmHost', '$canonicalName', '$driveModel', '$driveCapacity', '$driveIsSSD', '$driveHost',
                            '$driveController', '$driveNumber', '$driveRDM', '$driveTempHigh', '$driveTempLow', 1 )"
                    $sql.ExecuteNonQuery() | Out-Null
                } catch {
                    Write-Error -Message "Could not add data for '$canonicalName' into database. `nError: $( 
                        $_.Exception.Message )"
                }
            } else {
                Write-Host "Drive $driveId is already in Database"
            }
        }
    }

    # Compares all drives in ExtendedSmartData with those found in the database.
    $drivesPresentInDb = @()

    $sql.CommandText = 'SELECT `Canonical_Name` FROM `DriveInfo`'
    $reader = $sql.ExecuteReader()

    while( $reader.Read() ){ 
        $drivesPresentInDb += $reader.GetString( 0 )
    }

    $reader.Close()

    $drivesMissing = ( Compare-Object -ReferenceObject $ExtendedSmartData.'Drive CanonicalName' -DifferenceObject $drivesPresentInDb |
        Where-Object { $_.SideIndicator -eq "=>" } ).InputObject

    # for each drive missing, update the database with 0 (which equals removed).
    foreach ( $driveMissing in $drivesMissing ){
        $sql.CommandText = "UPDATE `DriveInfo` SET `IsPresent` = 0 WHERE `Canonical_Name` = '$driveMissing'"

        try {
            $sql.ExecuteNonQuery() | Out-Null
        } catch {
            Write-Error "Could not set '$driveMissing' as removed. `nError: $( $_.Exception.Message )"
        }
    }

    Return $psVersionNo

}
