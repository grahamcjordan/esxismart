
function Connect-ESXi {

    <#

        .SYNOPSIS
            Connects to ESXi using PowerCLI.

        .PARAMETER ESXiUser
            User account to log into ESXi. Default is root.

        .PARAMETER ESXiPass
            [MANDATORY] Password for $ESXiUser account in securestring format.

        .PARAMETER ESXiHost
            [MANDATORY] IP Address or Hostname of ESXi Host.

        .EXAMPLE
            Connect-ESXi -ESXiPass $( "ChangeMe!" | ConvertTo-SecureString -AsPlainText -Force ) -ESXiHost "ESXi"

            Connects to the DNS friendly name ESXi using the default root user, and very visible password ChangeMe!

        .EXAMPLE
            [hashtable]$esxiHash = @{
                ESXiPass = $( "ESXiPassword!" | ConvertTo-SecureString -AsPlainText -Force )
                ESXiUser = "SmartUser"
                ESXiHost = "172.16.0.2"
                OS       = "linux"
            }

            Connect-ESXi @esxiHash

            Will connect to the ESXi Host 172.16.0.2 using the non-standard root user SmartUser, and really crap password 
            ESXiPassword!
            As OS is set to Linux, will not check if an NTUser is admin when trying to install modules (if needed).


        .NOTES
            Author: Graham Jordan

    #>

    [cmdletbinding()]

    Param(

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$ESXiUser = 'root',

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [securestring]$ESXiPass,

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [string]$ESXiHost

    )

    $powerCLIInstalled = Get-Module -ListAvailable -Name "VMWare.PowerCLI"

    if ( !$powerCLIInstalled ){
        Write-Output "VMWare PowerCLI is not installed. Will attempt to install."

        # Installs PowerCLI from the PowerShell Gallery and ignores certificate validation.
        try {
            Install-Module -Name VMWare.PowerCLI -ErrorAction Stop -Force
            Write-Output "PowerCLI successfully installed."
            Write-Output "Setting PowerCLI to ignore SSL certificate errors."
            [void]( Set-PowerCLIConfiguration -InvalidCertificateAction ignore -ParticipateInCeip:$false -Confirm:$false -ErrorAction Stop )
        } catch {
            Write-Error "Cannot install 'VMWare.PowerCLI' and set InvalidCertificateAction to false. `nError $( $_.Exception.Message )"
        }
    }

    try {
        $esxiCreds = New-Object -TypeName pscredential( $ESXiUser, $ESXiPass )

        $connection = Connect-VIServer -Server $ESXiHost -Credential $esxiCreds -ErrorAction Stop
        Write-Output "Connected to $( $connection.Name ) on port $( $connection.Port ) as '$( $connection.User )'"
    } catch {
        Write-Error "Cannot connect to $ESXiHost. `nError is $( $_.Exception.Message )"
        throw
    }   
}
