
function Start-SmartDataScript {

    <#

        .SYNOPSIS
            Root Module for extracting data out of ESXi and pumping into SQL.

        .PARAMETER DriveExclusions
            Any drive where output is not desired (USB drives).

        .PARAMETER ESXiUser
            Username for ESXi. Default is Root.

        .PARAMETER ESXiPass
            [MANDATORY] Password for ESXiUser. In securestring format. If running function from a prompt, must use:
            Start-SmartDataScript -ESXiPass $( "MyESXiPass!" | ConvertTo-SecureString -AsplainText -Force )

        .PARAMETER ESXiHost
            [MANDATORY] IP or hostname for ESXi host.

        .PARAMETER SQLUser
            Username for MySQL. Defaults to Root.

        .PARAMETER SQLPass
            [MANDATORY] Password for SQLUser. In securestring format. If running function from a prompt, must use:
            Start-SmartDataScript -SQLPass $( "MySQLPass!" | ConvertTo-SecureString -AsplainText -Force )

        .PARAMETER SQLHost
            IP or hostname for computer with MySQL installed.

        .PARAMETER SQLPort
            Port to connect MySQL server. Defaults to 3306.

        .PARAMETER SQLSslType
            If your database is secure, an ssl encryption method is required. The default is none.

        .PARAMETER SQLDatabaseName
            Name of SQL database.

        .PARAMETER Email
            Email address you're both sending to and authentication from.

        .PARAMETER EmailPass 
            If you've not exported a cliXml file at this point, but you've provided an Email address and password,
            the script will generate said file for you. Ideal if you run this script manually once before scheduling.

        .PARAMETER HTMLTemplate
            Path to HTML Template file used to form the base email.

        .PARAMETER EMailPassPath
            Folder Path to cliXml file containing email credentials. File will be created in temp directory if Email
            and EmailPass provided but this is left null.

        .PARAMETER MailPort
            Microsoft, Gmail and Yahoo all use port 587 to send email. If a personal mail provider uses a different port
            then provide it here.

        .EXAMPLE
            Start-SmartDataScript

            Runs core module prompting for ESXi and SQL passwords.

        .EXAMPLE
            Start-SmartDataScript -Email test@contonso.com -EmailPass 5up3r53cur3P@55w0rd 

            Runs core module prompting for ESXi and SQL passwords, but will enable email alerts.

        .NOTES
            Author: Graham Jordan

            The default setup has XAMPP users in mind. The default port of MySQL on version 3.2.4 (testing version),
            is 3306. If this isn't working, check C:\xampp\mysql\bin\my.ini and look for port=
            If you're using a Synology device version 6 onwards you're likely using MariaDB10. In which case the port
            has changed to 3307. Either update the param below or set the param in the function call.
            Also, on Synology, the default setup will not allow root to connect remotely, therefore you should create
            a new user account with host name connection privileges as %.

        .TODO
            If a hard drive is added to the server and not to a machine before the script runs, DriveInfo will update
            as IsPresent = 0. Initialize-SQLSmartDatabase will need updating to change DriveInfo database based on any
            drive changes. I.E RDM file names, Controller Location etc. 
            Also, make sure drive temp low is not equal or lower than drive temp high. Not possible on website but 
            could be altered at database level.

    #>

    [cmdletbinding()]

    Param(

        [Parameter( Mandatory = $false )]
        [array]$DriveExclusions = @( "Ultra Fit" ),

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$ESXiUser = 'root',

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [securestring]$ESXiPass,

        [Parameter( Mandatory = $true )]
        [ValidateScript({
            if ( !( Test-Connection $_ -Count 1 ) ){
                throw "Unable to ping ESXi Host $_"
            } else { 
                $true 
            }
        })]
        [string]$ESXiHost,

            # # # MySql

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLUser = 'root',

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [securestring]$SQLPass,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLHost = '127.0.0.1',

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [int]$SQLPort = 3306,

        [Parameter( Mandatory = $false )]
        [ValidateSet( 'None', 'Preferred', 'Required')]
        [string]$SQLSslType = "None",

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLDatabaseName = 'ESXiDrives',

        [Parameter( Mandatory = $false )]
        # Simple regex to capture _most_ email addresses. https://stackoverflow.com/a/719543/5681924
        [ValidateScript({ $_ -match "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" })]
        [mailaddress]$Email,

        [Parameter( Mandatory = $false )]
        [string]$EmailPass,

        [Parameter( Mandatory = $false )]
        [ValidateScript({ Test-Path $_ -IsValid })]
        [string]$EmailPassPath,

        [Parameter( Mandatory = $false )]
        [string]$HTMLTemplate = "$( $PSScriptRoot )\template.html",

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [int]$MailPort = 587

    )

    # checks to see what OS we're running on.
    if ( $env:HOMEPATH ){
        $os = 'Windows'
        
        # Checks you're running as a local admin. Will fail otherwise.
        if ( -not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent(
            )).IsInRole( [Security.Principal.WindowsBuiltInRole] "Administrator" )){
            Write-Error "You are not running this as local administrator. Run again in an elevated prompt." 
            Throw "You are not running this as local administrator. Run again in an elevated prompt."
        }
    } elseif ( $env:HOME ){
        $os = "linux"
    }

    try {
        [hashtable]$connectESXi = @{
            ESXiUser = $ESXiUser
            ESXiPass = $ESXiPass
            ESXiHost = $ESXiHost
        }

        # Connects to ESXi using the hashtable generated above.
        Connect-ESXi @connectESXi

        # With an open connection to ESXi, retrieves SMART data,
        $smartData = Get-SmartData

        if ( $null -ne $smartData[0].Host ){
            Write-Output "Smart Data retrieved."
        } else {
            Write-Error "Could not retrieve smart data."
            Start-Sleep -Seconds 10
            Exit
        }

        # Using the open connection to ESXi, retrieves disk and host data.
        $vmDisksData = Get-VMDiskInformation

        [hashtable]$getExtendedSmartData = @{
            SmartData       = $smartData
            VMDisksData     = $vmDisksData
        }

        # Combines SMART data with disk/host data.
        $extendedSmartData = Get-ExtendedSmartData @getExtendedSmartData

        [hashtable]$connectMySQL = @{
            SQLUser = $SQLUser
            SQLPass = $SQLPass
            SQLHost = $SQLHost
            SQLPort = $SQLPort
        }

        # Using the hashtable above, connects to MySql
        $sqlConnection = Connect-MySQLforSmart @connectMySQL

        [hashtable]$initializeSqlSmartDatabase = @{
            SQLConnection     = $sqlConnection
            SQLDatabaseName   = $SQLDatabaseName
            ExtendedSmartData = $extendedSmartData
        }

        # If no database has been created, adds a new one. 
        # If drives have been added or removed, or if upgrading to version 2x, will alter the database accordingly.
        $psVersionNo = Initialize-SqlSmartDatabase @initializeSqlSmartDatabase

        # Adds Extended Smart Data to database.
        Add-ToSmartDB @initializeSqlSmartDatabase

        # If the database is at version 3x (which means the website is also at version 3x) then pull ESXi CPU, Memory, and Datastore data
        if ( $psVersionNo -ge 3 ){
            [hashtable]$addESXiHardwareData = @{
                ESXiHost          = $ESXiHost
                SqlConnection     = $sqlConnection
                SQLDatabaseName   = $SQLDatabaseName
                ExtendedSmartData = $extendedSmartData
            }

            Add-ESXiHardwareData @addESXiHardwareData

        } elseif ( $psVersionNo.GetType().Name -ne "Int32" ){
            Write-Warning ( "An error occured whilst checking the database schema. Manually log into MySql, " +
                "if table 'ESXiData' exists under '$SQLDatabaseName' then change 'PsVersionNo' on table 'ConfigSettings'" +
                " to 3. `nIf not, please report this to 'elmuziko' on 'Xpenology'" )
        }

        # If an email address has been provided...
        if ( $Email ){

            #... First validate email based parameters.
            [hashtable]$startEmailValidation = @{
                Email = $Email
                OS    = $os
            }

            if ( $EmailPass ){
                $startEmailValidation.Add( "EmailPass", $EmailPass )
            }

            if ( $EmailPassPath ){
                $startEmailValidation.Add( "EmailPassPath", $EmailPassPath )
            }

            try {
                # Perform email parameter validation.
                $emailcliXml = Start-EmailValidation @startEmailValidation
            } catch {
                $_
            }

            if ( $emailcliXml ){
                # build hashtable to pass over to AlertChecks.
                [hashtable]$startAlertChecks = @{
                    SQLConnection     = $sqlConnection
                    SQLDatabaseName   = $SQLDatabaseName
                    ExtendedSmartData = $ExtendedSmartData
                    Email             = $Email
                    EmailPassPath     = $emailcliXml
                    HTMLTemplate      = $HTMLTemplate
                    MailPort          = $MailPort
                }

                Start-AlertChecks @startAlertChecks
            }
        }

        # Disconnect from SQL and ESXi
        $sqlConnection.Close()
        Disconnect-ViServer -Confirm:$false -ErrorAction SilentlyContinue

        # If you're running Windows, will attampt to make a scheduled Task for you.
        if ( $os -eq "Windows" ){
            $schedTasks = Get-ScheduledTask | Where-Object { $_.Actions.Task -eq "SMART Data Grab" }

            if ( !$schedTasks ){
                $schedTasks = Get-ScheduledTask | Where-Object { $_.Actions.Execute -match "PowerShell" }
            }

            # Params to send if no schedule task has started 
            if ( !$schedTasks ){
                [hashtable]$scheduledTaskHash = @{
                    ESXiUser = $ESXiUser
                    ESXiPass = $ESXiPass
                    ESXiHost = $ESXiHost
                    SQLUser  = $SQLUser
                    SQLPass  = $SQLPass
                    SQLHost  = $SQLHost
                    SQLPort  = $SQLPort
                }

                # If you've ran the script with email parameters, will include email alerts on scheduled task.
                if ( $emailcliXml ){
                    $scheduledTaskHash.Add( "Email", $Email )
                    $scheduledTaskHash.Add( "EmailPassPath", $emailcliXml.EmailPassPath )
                }

                New-SmartScheduledTask @$scheduledTaskHash
            }
        } 
    } catch {
        Disconnect-ViServer -Confirm:$false -ErrorAction SilentlyContinue

        if ( $sqlConnection ){
            $sqlConnection.Close()
        }
        Write-Error "An error occured during the Start-SmartData script. `nError:$( $_.Exception.Message )"
        Start-Sleep -Seconds 10
        throw
    }
}