
function Add-ESXiHardwareData {

    <#

        .SYNOPSIS
           Retrieves ESXi CPU, Memory and Datastore data.

        .PARAMETER ESXiHost
            [MANDATORY] IP or hostname for ESXi host.

        .PARAMETER SQLConnection
            [MANDATORY] Requires an SQL connection. Should be in the form of:
                $sqlConnection = [MySql.Data.MySqlClient.MySqlConnection]@{ 
                    ConnectionString="server=$SqlHost;port=3306;uid=$SQLUser;pwd=$Password" }

        .PARAMETER SQLDatabaseName
            Name of SQL database.

        .PARAMETER ExtendedSmartData
            [MANDATORY] PowerShell Object Array containing drive data. Used for the date attribute.

        .EXAMPLE
           Add-ESXiHardwareData -ESXiHost 192.168.0.2 -SqlConnection $SqlConnection -ExtendedSmartData $ExtendedSmartData

           The above example is effectivly how to function is ran as part of the ESXiSMART script. $SqlConnection and 
           $ExtendedSmartData have already been written.

        .NOTES
           Author: Graham Jordan

        TODO
            This only addresses a single ESXi host, whilst the rest of the script addresses multipule. May need to visit

   
   #>

    Param (

        [Parameter( Mandatory = $true )]
        [string]$ESXiHost,

        [Parameter( Mandatory = $true )]
        [System.Data.Common.DbConnection]$SQLConnection,

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [string]$SQLDatabaseName = "ESXiDrives",

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$ExtendedSmartData

    )

    # Gets CPU, and Memory Information from the ESXi host
    $esxiHostData = Get-VMHost -Name $ESXiHost -ErrorAction SilentlyContinue | Select-Object @{ 
           Name       = 'CPU GHz Capacity'
           Expression = { [math]::Round( $_.CpuTotalMhz / 1000, 2 )}},
        @{ Name       = 'CPU GHz Used'
           Expression = { [math]::Round( $_.CpuUsageMhz / 1000,2 )}},
        @{ Name       = 'CPU GHz Free' 
           Expression = { [math]::Round(( $_.CpuTotalMhz - $_.CpuUsageMhz ) / 1000, 2)}},
        @{ Name       = 'Memory Capacity GB' 
           Expression = { [math]::Round( $_.MemoryTotalGB, 2 )}},
        @{ Name       = 'Memory Used GB'
           Expression = { [math]::Round( $_.MemoryUsageGB, 2 )}},
        @{ Name       = 'Memory Free GB'
           Expression = { [math]::Round(( $_.MemoryTotalGB - $_.MemoryUsageGB ), 2 )}}

    # Gets datstore info.
    $dataStores = Get-DataStore -ErrorAction SilentlyContinue

    # Works out total capacity and total free space of datastore(s)
    try {
        $sum1 = $dataStores.CapacityGB -join '+'
        $capacity = Invoke-Expression $sum1

        $sum2 = $dataStores.FreeSpaceGB -join '+'
        $freeSpace = Invoke-Expression $sum2
        $freeSpace = [math]::Round( $freeSpace, 2 )
    } catch {
        Write-Error "Datastore information cannot be obtained. ESXi Data injection will be inconsistent."
    }

    # If connection to SQL database isn't open, open.
    if ( $SQLConnection.State -ne "Open" ){
        try {
            $SQLConnection.Open()
            Write-Output "Opened connection to $( $SQLConnection.DataSource )."
        } catch {
            throw "Cannot connect to $( $SQLConnection.DataSource ). Error:$( $_.Exception.Message )"
        }
    }

    try {
        # Sets sql database commands base.
        $sql = New-Object MySql.Data.MySqlClient.MySqlCommand
        $sql.Connection = $SQLConnection

        $sql.CommandText = "USE $SQLDatabaseName"
        $Sql.ExecuteNonQuery() | Out-Null

        # Double checks EsxiData exists
        $sql.CommandText = "Select 1 from `EsxiData` LIMIT 1"
        $reader = $sql.ExecuteReader()
        $reader.Close()

        # Inserts (overwrites) ESXi data into EsxiData database.
        try { 
            $sql.CommandText = "UPDATE EsxiData SET 
                MemTotal = '$( $esxiHostData.'Memory Capacity GB' )',
                MemUsage = '$( $esxiHostData.'Memory Used GB' )',
                CPUTotal = '$( $esxiHostData.'CPU GHz Capacity' )',
                CPUUsage = '$( $esxiHostData.'CPU GHz Used' )',
                DataStoreTotal = '$capacity',
                DataStoreUsage = '$freeSpace',
                Date = '$( $ExtendedSmartData[0].Date )'"

            $sql.ExecuteNonQuery() | Out-Null
        } catch {
            Write-Error -Message "Could not set ESXi Data. `nError: $( 
                $_.Exception.Message )"
        }
    } catch {
        Write-Error -Message "Could not query/set ESXi Data. `nError: $(
            $_.Exception.Message )"
    }
}