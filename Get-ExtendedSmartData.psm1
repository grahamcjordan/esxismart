
function Get-ExtendedSmartData {

     <#

        .SYNOPSIS
            Combines two data sources into one array.

        .PARAMETER SmartData
            [MANDATORY] SmartData is a PowerShell object containing drive information. 
            Can be obtained by running $SmartData = Get-SmartData

        .PARAMETER VMDiskData
            [MANDATORY] VMDiskData is a PowerShell object containing ESXi drive configuration.
            Can be obtained by runnunb $VMDisksData = Get-VMDiskInformation

        .EXAMPLE
            Get-ExtendedSmartData -SmartData $SmartData -VMDisksData $VMDisksData

            Creates an array with extended drive information; a combintation of SMART data and hard drive technical details.

        .NOTES
            Author: Graham Jordan
    
    #>

    [cmdletbinding()]

    param(

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$SmartData,

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$VMDisksData

    )

    [array]$extendedSmartData = @()

    foreach ( $drive in $SmartData ){
        if ( $VMDisksData.'Drive CanonicalName' -contains $drive.DriveName ){
            $driveNumber = ( $VMDisksData | Where-Object { $_.'Drive CanonicalName' -eq $drive.DriveName } ).'Drive Number'
            $driveHost   = ( $VMDisksData | Where-Object { $_.'Drive CanonicalName' -eq $drive.DriveName } ).'Drive Host Machine'
            $driveCL     = ( $VMDisksData | Where-Object { $_.'Drive CanonicalName' -eq $drive.DriveName } ).'Controller Location'
            $driveRDM    = ( $VMDisksData | Where-Object { $_.'Drive CanonicalName' -eq $drive.DriveName } ).'RDM File Name'
        } else { 
            $driveNumber = "Not attached to machine"
            $driveHost   = "Not attached to machine"
            $driveCL     = "Not attached to machine"
            $driveRDM    = "Not an RDM hard disk"
        }

        $extendedSmartData += [PSCustomObject]@{
            Host                  = $drive.Host
            'Drive CanonicalName' = $drive.DriveName
            'Drive Capacity'      = $drive.Capacity
            'Drive Model'         = $drive.Model
            SSD                   = $drive.IsSSD
            Health                = $drive.Health
            WriteError            = $drive.WriteError
            WriteErrorThreshold   = $drive.WriteErrorThreshold
            WriteErrorWorst       = $drive.WriteErrorWorst
            ReadError             = $drive.ReadError
            ReadErrorThreshold    = $drive.ReadErrorThreshold
            ReadErrorWorst        = $drive.ReadErrorWorst
            PowerCycleCount       = $drive.PowerCycleCount
            ReallocatedSectors    = $drive.ReallocatedSectors
            RawReadErrorRate      = $drive.RawReadErrorRate
            Temperature           = $drive.Temperature
            DriveTempHigh         = $drive.DriveTempHigh
            DriveTempLow          = $drive.DriveTempLow
            Date                  = $drive.Date
            'Drive Uptime'        = $drive.DriveUpTime
            'Drive Number'        = $driveNumber
            'Drive Host'          = $driveHost
            'Controller Location' = $driveCL
            'RDM File Name'       = $driveRDM
        }
    }

    Return $extendedSmartData

}