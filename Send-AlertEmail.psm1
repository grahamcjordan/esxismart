
function Send-AlertEmail {

    <#

        .SYNOPSIS
            Sends email when an alert has been detected.

        .DESCRIPTION
            For each error found in 'Start-AlertChecks' function, will send an alert email to an email address
            specified.

        .PARAMETER Errors
            [MANDATORY] $Errors should be an array containing collected data surrounding a drive error.

        .PARAMETER Email
            [MANDATORY] Email address you're both sending to and authentication from.

        .PARAMETER EMailPassPath
            [MANDATORY] If running from a Windows Machine, this is a scrambled/encrypted file containing your
            email password. This can only be opened by you, on this machine. Any other user or any other
            computer and this cannot be decrypted.

        .PARAMETER HTMLTemplate
            Path to HTML Template file used to form the base email.

        .PARAMETER MailPort
            Microsoft, Gmail and Yahoo all use port 587 to send email. If a personal mail provider uses a different port
            then provide it here.

    #>

    [cmdletbinding()]

    param(

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [array]$Errors,

        [Parameter( Mandatory = $true )]
        # Simple regex to capture _most_ email addresses. https://stackoverflow.com/a/719543/5681924
        [ValidateScript({ $_ -match "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" })]
        [mailaddress]$Email,

        [Parameter( Mandatory = $true )]
        [ValidateNotNullOrEmpty()]
        [string]$EmailPassPath,

        [Parameter( Mandatory = $false )]
        [string]$HTMLTemplate = "$( $PSScriptRoot )\template.html",

        [Parameter( Mandatory = $false )]
        [ValidateNotNullOrEmpty()]
        [int]$MailPort = 587

    )

    # Sets the mail host dependant on mail provider.
    # IF YOU HAVE A PERSONAL EMAIL ADDRESS, YOU'LL NEED TO ADD A SWITCH AS BELOW.
    switch ( $Email.Host.Split( "." )[0] ) {
        "Gmail" { $mailHost = "smtp.gmail.com" }

        "Hotmail" { $mailHost = "smtp.live.com" }
        "Outlook" { $mailHost = "smtp.live.com" }

        "Yahoo" { $mailHost = "smtp.mail.yahoo.com" }
    }

    # Imports previously exported email credentials.
    $emailCredentials = Import-Clixml -Path $EmailPassPath

    try {
        # Builds Mail Sending Object.
        $mailer = New-Object Net.Mail.SMTPClient
        $mailer.Host = $mailHost
        $mailer.Port = $MailPort
        $mailer.Credentials = $emailCredentials
        $mailer.EnableSsl = $true

        # Builds an email
        $message = New-Object Net.Mail.MailMessage( $emailCredentials.UserName, $emailCredentials.UserName )
        $message.Sender = $emailCredentials.UserName
        $message.ReplyTo = $emailCredentials.UserName
    } catch {
        Write-Error "A required value is missing in order to create mail object. Cannot continue"
        Exit
    }

    $message.Subject = "ESXi SMART DATA: $( $Errors.Subject )"

    # HTML email. Better formatting but can sometimes be rejected by mail host as spam.
    $htmlEmail = Get-Content -Path $HTMLTemplate

    $htmlEmail = $htmlEmail -replace "%%ERRORSUBJECT%%", $Errors.Subject
    $htmlEmail = $htmlEmail -replace "%%ERRORLINE%%", $Errors.Error

    # traverse the $errors object, and add html code for each row.
    $Errors | ForEach-Object { 
        $_.psobject.Properties | Where-Object { $_.MemberType -eq "NoteProperty" -and $_.Name -ne "Error" } | Foreach-Object {
            $htmlEmail = $htmlEmail -replace "%%EDITME%%", @"
                $(
                    if ( $_.Name -eq "Subject" ){
                    } elseif ( $_.Name -eq "Capacity" ){ 
                        "<li>$( $_.Name )<ul> 
                            <li>$( $_.Value ) GB</li>
                        </ul>"
                    } elseif ( $_.Name -eq "Temperature" ){
                        "<li>$( $_.Name )<ul> 
                            <li> $( $_.Value ) C</li>
                        </ul>"
                    } else {
                        "<li>$( $_.Name )<ul> 
                            <li>$( $_.Value )</li>
                        </ul>"
                    }
                )
            %%EDITME%%
"@ 
        }
    }

    # Remove last place holder point and inbed html content into email.
    $htmlEmail = $htmlEmail -replace "%%EDITME%%", "</li>"
    $body = $htmlEmail
    $message.IsBodyHtml = $true

    # Adds the body then sends the email.
    $message.Body = $body
    try {
        $mailer.Send( $message )
    } catch {
        Write-Error $( $_.Exception.Messgae )
        Start-Sleep -Seconds 10
        throw 
    }
}