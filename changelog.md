
### Version 3.0.0
 - Update
    - Added Modules
        - Set ESXiData
            New script will update Database Table ESXiData with Memory usage, CPU usage, and Datastore Usage.
        - New-SmartScheduledTask
            If on a Windows Computer, will generate a scheduled task to be ran every thirty minutes for you. 
            Will encrypt passwords so as to not expose ESXi, MySQL or web-email passwords in plain text.
    - Updated Modules
        - Start-SmartDataScript
            Records details of ESXISMART version no, and if version 3, calls new ESXiData script.
            Removed MailProvider parameter. Not needed. If not using Google, Yahoo or Outlook to send mails, 
            SMTP details need hardcoding in Send-AlertEmail anyway.
        - Connect-ESXi
            Added defensive coding for Linux users. In Windows, installing the module needs to be done with 
            Administrator privlidges, Linux doesn't seem to care(??)
        - Connect-MySQLForSmart
            Renamed module as appeared to be conflicting. 
            No longer requires Oracle install, instead will download SimplySQL from PowerShell Gallery.
        - Initialize-SQLSmartDatabase
            Performs a database upgrade if web site and table 'ConfigSettings' -> WebVersionNo is at version 3.
        - Get-SmartData
            Detects drive when NVMe and sets the drive temperature to celcius.
        - Start-EmailValidation
            Moved the location of the .credential from file $env:TEMP to the same folder the script is ran from, as Windows
            Feature Updates appears to empty out the temp directory and delete the file.
    - Bug Fixes
        - Initialize-SQLSmartDatabase
            When a new drive is added, was not setting 'IsPresent' to 1.
        - Start-AlertChecks
            Had to declare $errors as null on each drive else would send an email for each drive, even if only one breached.
            Had to delcate drive Temperature as an int else would not compare values against threshold limits.
            Alerts were being sent when the min and max temps were met, not breached. Altered accordingly.

### Version 2.0.0

 - Update
 - Added Modules
    Start-EmailValidation
        Validates an email for alerts.
        If password provided will export a credentials file for future inmport.
        If credentials file exists will validate properties against provided email.
    Start-AlertChecks
        Will scan ExtendedSmartData against DriveInfo database for any threshold breaches or missing drives.
        If drive Health is no longer "OK" will pass extensive data over to Send-AlertEmail.
    Send-AlertEmail
        When an error event has been found on a hard drive, will send an email to address provided.
 - Added Files
    template.html
        HTML template for email alerts. 

### Version 1.0.1

    Set ESXiHost param to Mandatory in the following files:
        Start-SmartDataScript.psm1
        Connect-ESXi.psm1
    
    Additional updated files
        README.md
        ESXiSMART.psd1

### Version 1.0.0

 - Release

