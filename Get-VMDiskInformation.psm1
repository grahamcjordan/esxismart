
function Get-VMDiskInformation {

    <#

        .SYNOPSIS
            Retrieves RAW disk info from individual virtual machines.

        .DESCRIPTION
            Detects connected hard drive information on each virtual machine, and provides RAW drive details. 
            What machine the hard drive is connected to, the controller connection, drive letter, and RDM
            file path reported back to the script.

        .EXAMPLE
            Get-VmDiskInformation

        .NOTES
            Author: Graham Jordan
    
    #>

    [cmdletbinding()]

    param( )

    [array]$vmDisksData = @()

    # gets the VM objects and creates SMART arguments.
    $vmViews = Get-View -Viewtype VirtualMachine -Property Name, Config.Hardware.Device, Runtime.Host

    # checks variables
    if ( !$vmViews ){
        Write-Error "Cannot find any virtual machines."
        throw "Cannot find any virtual machines."
    }

    foreach ( $vmView in $vmViews ){
        # raw data on each disk attached to a vm.
        $vmHostStorage = Get-View -Id $vmView.Runtime.Host -Property Config.StorageDevice.ScsiLun

        # for each disk on a vm.
        $vmView.Config.Hardware.Device | Where-Object { $_ -is [VMware.Vim.VirtualDisk] } | Foreach-Object {
            $disk = $_

            $hardDiskRAW = $null
            [string]$controllerKey = $disk.ControllerKey
            $hardDiskRAW = $vmHostStorage.Config.StorageDevice.ScsiLun | Where-Object { $_.UUID -eq $disk.Backing.LunUuid }

            # only add data to array if connected hard drive is raw.
            if ( $hardDiskRAW.CanonicalName ){

                [hashtable]$vmDiskData = @{
                    'Drive CanonicalName' = $hardDiskRAW.CanonicalName
                    'Drive Model'         = $hardDiskRAW.Model
                    'Drive Capacity'      = [math]::Round( ( $disk.CapacityInBytes /1gb ), 2 )
                    'Drive Host Machine'  = $vmView.Name
                    'Drive Number'        = $disk.DeviceInfo.Label
                    'Controller Location' = "$( $controllerKey[$controllerKey.Length - 1] ):$( $disk.UnitNumber )"
                    'RDM File Name'       = $disk.Backing.FileName
                }

                # add per disk output to array
                $vmDisksData += New-Object -Type PsObject -Property $vmDiskData
            }
        }
    }

    if ( $null -ne $vmDisksData[0].'Drive CanonicalName' ){
        Write-Output "VM Disk Information gathered."
    } else {
        throw "Could not retrieve VM Disk Information."
        exit
    }

    Return $vmDisksData

}
