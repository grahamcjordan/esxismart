
function Start-EmailValidation {

    <#

        .SYNOPSIS
            Validates Email parameters passed into function from Start-SmartDataScript.

        .PARAMETER Email
            [MANDATORY] Email address you're both sending to and authentication from.

        .PARAMETER EmailPass 
            Password for mail provider.

        .PARAMETER EMailPassPath
            Path to cliXml file or folder. Does not have to exist at this point.

        .PARAMETER OS
            Checks if Windows or Linux. In Windows, Windows Defender can stop you writing to protected zones with 
            PowerShell. Unblock PowerShell temporarily. 

        .EXAMPLE
            Start-EmailValidation -Email bill.gates@contoso.com

            If a file called bill.gates.credential exists in the folder where the script is ran from, script will
            pass path the full path of file.
            If no file exists, the script will friendly error and skip Email Alerts.

        .EXAMPLE
            Start-EmailValidation -Email bill.gates@contoso.com -EmailPass MsDos4eva!

            Will forcefully create bill.gatescontoso.credential file in the folder where the script lives and return
            the full path.

        .EXAMPLE
            Start-EmailValidation -Email bill.gates@contoso.com -EmailPassPath /home/bill.gates/Downloads/SMART/

            Will check to see if bill.gatescontoso.credential exists in /home/bill.gates/Downloads/SMART/.
            If it does, then will return /home/bill.gates/Downloads/SMART/bill.gatescontoso.credential

            If not, will not perform Email Alerts.

        .EXAMPLE 
            Start-EmailValidation -Email bill.gates@contoso.com -EmailPassPath $env:TEMP\bill.gatescontoso.credential

            Will check to see if $env:TEMP\bill.gatescontoso.credential exists. If it does, it will return this very
            location, if it does not, will not perform Email Alerts.

        .EXAMPLE
            Start-EmailValidation -Email bill.gates@contoso.com -EmailPassPath "$( ( Get-Location ).Path 
                )\bill.gatescontoso.credential -EmailPass MsDosLives!

            Will check to see if bill.gatescontoso.credential exists in the folder where the script is ran from. 
            If it is, will check to see if the credential file matches the credentials passed. If not, will overwrite 
            credential file with a new one, then return full file path.

            If the file does not exist, script will create and return the full path.

        .NOTES
            Author Graham Jordan

            If you have protected files switched on, and this script is saved in that folder, then every time the script 
            runs, Windows Defender will ping to say "PowerShell tried to write to x folder".
            If you're running this on a machine you actively use, I will imagine this is going to piss you off. A lot.
            But. it can't be helped I'm afraid. There are checks in place to make sure the credentials file matches
            the email addressed passed over and if not, make a new file. You could if you wish, edit this file and remove
            the IF ( $OS eq "Windows" ) section. Which will work fine as long as your email never changes, or your password
            is never updated.

            Realistically this should only be a problem if you use this on a live machine with a Windows Live Logon account.
            If you have a Windows machine with a local logon, I believe Windows Defender doesn't automatically switch
            protected folders on for said devices so you will be ok. 

            In any case, hit me up ( Graham Jordan ) on Xpenology Forums if you are getting any unusual behaviour.

    #>

    [cmdletbinding()]

    Param( 

        [Parameter( Mandatory = $true )]
        # Simple regex to capture _most_ email addresses. https://stackoverflow.com/a/719543/5681924
        [ValidateScript({ $_ -match "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" })]
        [mailaddress]$Email,

        [Parameter( Mandatory = $false )]
        [ValidateLength( 8,30 )]
        [string]$EmailPass,

        [Parameter( Mandatory = $false )]
        [string]$EmailPassPath,

        [Parameter( Mandatory = $false )]
        [string]$OS = "Windows"

    )

    $clixmlName   = "$( $Email.User )$( $Email.Address.Split( "@" )[1].Split( "." )[0] ).credential"
    $saveLocation = ( Get-Location ).Path

    # If running Windows, make sure you can write to the folder in which script is being kept.
    if ( $os -eq "Windows" ){

        try {
            "testFile" | Out-File -FilePath "$saveLocation\test.ps1" -ErrorAction Stop -Force
            [void]( Remove-Item -Path "$saveLocation\test.ps1" -Force )
        } catch {
            # Found on my own machine, Windows Defender locks my folder for any potential harmful files. So allow PowerShell 
            # through, and exclude the folder. We'll reset this later. Works when running as admin. Which you have to be fpr ESXi.
            Add-MpPreference -ControlledFolderAccessAllowedApplications 'c:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe' -Verbose
            Start-Sleep -Seconds 5
            Add-MpPreference -ExclusionPath $saveLocation -Force -Verbose
            Start-Sleep -Seconds 5
            $alteredMpPreference = $true
        }
    }

    if ( $EmailPassPath ){

        # EmailPassPath already exists as an absolute file.
        if ( ( Test-Path -Path $EmailPassPath -Include *.credential ) -eq $true ){
            # Imports previously exported email credentials.
            $emailCredentials = Import-Clixml -Path $EmailPassPath

            # Check Email entered as a parameter is the same as that found in cliXml.
            if ( $Email.Address -ne $emailCredentials.UserName ){
                Write-Warning "Email address provided does not match email address in cliXml."

                # Overwrite existing cliXml file with new credentials.
                if ( $EmailPass ){
                    $creds = New-Object pscredential( $Email, ( $EmailPass | ConvertTo-SecureString -Force -AsPlainText ))
                    $creds | Export-Clixml -Path $EmailPassPath -Force
                } else {
                    Write-Error "No password provided. Skipping creation of cliXml file. Cannot run alert checks."
                    $EmailPassPath = $null
                }
            }
        # When an absolute path has been provided, but the file does not exist.
        } elseif ( ( Test-Path -Path $EmailPassPath -Include *.credential -IsValid ) -eq $true ){
            # Export cliXml file with new credentials.
            if ( $EmailPass ){
                $creds = New-Object pscredential( $Email, ( $EmailPass | ConvertTo-SecureString -Force -AsPlainText ))
                $creds | Export-Clixml -Path $EmailPassPath
            } else {
                Write-Error "No password provided. Skipping creation of cliXml file. Cannot run alert checks."
                $EmailPassPath = $null
            }
        # When the EmailPassPath is just a folder...
        } elseif ( ( Test-Path -Path $EmailPassPath -PathType Container ) -eq $true ){

            #... if a password is provided then export cliXml file.
            if ( $EmailPass ){
                $creds = New-Object pscredential( $Email, ( $EmailPass | ConvertTo-SecureString -Force -AsPlainText ))
                $creds | Export-Clixml -Path "$EmailPassPath\$clixmlName" -Force

                [string]$EmailPassPath = "$EmailPassPath\$clixmlName"
            } else {
                Write-Error "No password provided. Skipping creation of cliXml file."
                $EmailPassPath = $null
            }
        } else {
            Write-Error "`$EmailPassPath as '$EmailPassPath' is an unknown string. Cannot run alert checks."
            $EmailPassPath = $null
        }
    # No EmailPassPath provided, but do have a password.
    } elseif ( $EmailPass ){
        [string]$EmailPassPath = "$saveLocation\$clixmlName"

        $creds = New-Object pscredential( $Email, ( $EmailPass | ConvertTo-SecureString -Force -AsPlainText ))
        $creds | Export-Clixml -Path $EmailPassPath -Force
    # Only email provided as per mandatory requirement. Will check to see if a credential file exists in default 
    # location, and if so, that the email matches that passed over. 
    } else {
        [string]$EmailPassPath = "$saveLocation\$clixmlName"

        try {
            $emailCredentials = Import-Clixml -Path $EmailPassPath -ErrorAction Stop

             # Check Email entered as a parameter is the same as that found in cliXml.
            if ( $Email.Address -ne $emailCredentials.UserName ){
                Write-Error "Email address '$( $Email.User )' does not match email address in cliXml $( 
                    $emailCredentials.UserName ). Cannot run alert checks."

                $EmailPassPath = $null
            }
        } catch {
            # Version 2 of script saved credential file in $env:TEMP. Attempt to move it out into script folder.
            if ( ( Test-Path -Path "$env:TEMP\$clixmlName" ) -eq $true ){
                try {
                    Copy-Item -Path "$env:TEMP\$clixmlName" -Destination $saveLocation -Force -ErrorAction Stop
                    $emailCredentials = Import-Clixml -Path $EmailPassPath -ErrorAction Stop

                    # Check Email entered as a parameter is the same as that found in cliXml.
                    if ( $Email.Address -ne $emailCredentials.UserName ){
                        Write-Error "Email address '$( $Email.User )' does not match email address in cliXml $( 
                            $emailCredentials.UserName ). Cannot run alert checks."

                        $EmailPassPath = $null
                    }
                } catch {
                    # 
                    $eMessage = ( "Only Email address provided. No cliXml file found at '$saveLocation'." +
                        "If cliXml exists in a different folder, re-run script and pass over as -EmailPassPath parameter." +
                        "Cannot run alert checks." )

                    Write-Error $eMessage 
                    $EmailPassPath = $null
                }
            }
        }
    }

    # If Windows Defender was hacked, undo the changes
    if ( $alteredMpPreference ){
        Remove-MpPreference -ExclusionPath $saveLocation -Force
        Remove-MpPreference -ControlledFolderAccessAllowedApplications 'c:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe' 
    }

    if ( $EmailPassPath ){
        Return $EmailPassPath
    }
}